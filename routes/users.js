var express = require('express');
var router = express.Router();
var async = require("async");
var reveal = require('../common/reveal.js');

const REVEAL_API_URL = 'http://192.168.16.72/PortalAPI/cxReveal.asmx/';


/** 
* @Description: Retrieve Last Five Sessions from Tealeaf
* @Response: List of Sessions
* @Path: /getlastfivesessions
* @Type: POST
* @Response Format: application/xml
* @Developer: Waheed Ahmed
*/
router.post('/getlastfivesessions', function(req, res, next) {

  if(req.body && req.body.email && req.body.username && req.body.password) {

    var emailId = req.body.email;
    var onlyLetterEmailId = emailId.replace(/[^a-zA-Z ]/g, " ");
    var credentials = {
      UserName: req.body.username,
      Password: req.body.password
    };

    // call helper Reveal Methods to get Sessions List. Array contains Methods. Each method runs after other in sequence.
    async.waterfall([
      async.apply(reveal.authenticate, credentials),
      reveal.verifyAuthentication,
      reveal.getSearchSortFields,
      async.apply(reveal.getLastFiveSessions, emailId, onlyLetterEmailId)
    ], function (err, result) {
      if(!err) {
        if(result != null) {
          res.header("Content-Type",'application/xml');
          res.send(result);
        } else {
          res.send();
        }
      } else {
        console.log("error: ", err);
        res.send();
      }
    });
  }
});

/** 
* @Description: Retrieve Last Seven Days Sessions from Tealeaf
* @Response: List of Sessions
* @Path: /getlast7dsessions
* @Type: POST
* @Response Format: application/xml
* @Developer: Waheed Ahmed
*/
router.post('/getlast7dsessions', function(req, res, next) {

  if(req.body && req.body.email && req.body.username && req.body.password) {

    var emailId = req.body.email;
    var onlyLetterEmailId = emailId.replace(/[^a-zA-Z ]/g, " ");
    var credentials = {
      UserName: req.body.username,
      Password: req.body.password
    };

    // call helper Reveal Methods to get Sessions List. Array contains Methods. Each method runs after other in sequence.
    async.waterfall([
      async.apply(reveal.authenticate, credentials),
      reveal.verifyAuthentication,
      reveal.getSearchSortFields,
      async.apply(reveal.getLastSevenDaysSessions, emailId, onlyLetterEmailId)
    ], function (err, result) {
      if(!err) {
        if(result != null) {
          res.header("Content-Type",'application/xml');
          res.send(result);
        } else {
          res.send();
        }
      } else {
        console.log("error: ", err);
        res.send();
      }
    });
  }
});
module.exports = router;
