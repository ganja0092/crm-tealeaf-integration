var express = require('express');
var router = express.Router();
var async = require("async");
var reveal = require('../common/reveal.js');

const REVEAL_API_URL = 'http://192.168.16.72/PortalAPI/cxReveal.asmx/';


/** 
* @Description: Retrieve Last Five Sessions from Tealeaf
* @Response: List of Sessions
* @Path: /getlastfivesessions
* @Type: POST
* @Response Format: application/xml
* @Developer: Waheed Ahmed
*/
router.post('/getlastfivesessions', function(req, res, next) {

  if(req.body && req.body.email && req.body.username && req.body.password) {

    var emailId = req.body.email;
    var onlyLetterEmailId = emailId.replace(/[^a-zA-Z ]/g, " ");
    var credentials = {
      UserName: req.body.username,
      Password: req.body.password
    };
    // call helper Reveal Methods to get Sessions List. Array contains Methods. Each method runs after other in sequence.
    async.waterfall([
      async.apply(reveal.authenticate, credentials),
      reveal.verifyAuthentication,
      reveal.getSearchSortFields,
      async.apply(reveal.getLastFiveSessions, emailId, onlyLetterEmailId)
    ], function (err, result) {
      res.header("Content-Type",'application/json');
      if(!err) {
        if(result != null) {
          res.status(200).jsonp(result);
        } else {
          res.status(200).send();
        }
      } else {
        console.log("error: ", err);
        res.status(500).jsonp(err);
      }
    });
  } else {
    var err = reveal.errorResponse("Required Fields are missing from Request Body.");
    res.status(500).jsonp(err);
  }
});

/** 
* @Description: Retrieve Last Seven Days Sessions from Tealeaf
* @Response: List of Sessions
* @Path: /getlast7daysessions
* @Type: POST
* @Response Format: application/xml
* @Developer: Waheed Ahmed
*/
router.post('/getlastspecificdaysessions', function(req, res, next) {

  if(req.body && req.body.email && req.body.username && req.body.password) {

    var emailId = req.body.email;
    var onlyLetterEmailId = emailId.replace(/[^a-zA-Z ]/g, " ");
    var credentials = {
      UserName: req.body.username,
      Password: req.body.password
    };
    var searchDuration = req.body.searchDuration ? req.body.searchDuration : 7;
    console.log("searchDuration: ", searchDuration);
    // call helper Reveal Methods to get Sessions List. Array contains Methods. Each method runs after other in sequence.
    async.waterfall([
      async.apply(reveal.authenticate, credentials),
      reveal.verifyAuthentication,
      reveal.getSearchSortFields,
      async.apply(reveal.getLastSpecificDaysSessions, emailId, onlyLetterEmailId, searchDuration)
    ], function (err, result) {
      res.header("Content-Type",'application/json');
      if(!err) {
        if(result != null) {
          res.status(200).jsonp(result);
        } else {
          res.status(200).send();
        }
      } else {
        console.log("error: ", err);
        res.status(500).jsonp(err);
      }
    });
  } else {
    var err = reveal.errorResponse("Required Fields are missing from Request Body.");
    res.status(500).jsonp(err);
  }
});

/** 
* @Description: Retrieve Last Seven Days Sessions from Tealeaf
* @Response: List of Sessions
* @Path: /getactivesessions
* @Type: POST
* @Response Format: application/xml
* @Developer: Waheed Ahmed
*/
router.post('/getactivesessions', function(req, res, next) {

  if(req.body && req.body.email && req.body.username && req.body.password) {

    var emailId = req.body.email;
    var onlyLetterEmailId = emailId.replace(/[^a-zA-Z ]/g, " ");
    var credentials = {
      UserName: req.body.username,
      Password: req.body.password
    };
    var searchType = "active";
    // call helper Reveal Methods to get Sessions List. Array contains Methods. Each method runs after other in sequence.
    async.waterfall([
      async.apply(reveal.authenticate, credentials),
      reveal.verifyAuthentication,
      reveal.getSearchSortFields,
      async.apply(reveal.getActiveSessions, emailId, onlyLetterEmailId, searchType)
    ], function (err, result) {
      res.header("Content-Type",'application/json');
      if(!err) {
        if(result != null) {
          res.status(200).jsonp(result);
        } else {
          res.status(200).send();
        }
      } else {
        console.log("error: ", err);
        res.status(500).jsonp(err);
      }
    });
  } else {
    var err = reveal.errorResponse("Required Fields are missing from Request Body.");
    res.status(500).jsonp(err);
  }
});

/** 
* @Description: Retrieve Last Seven Days Sessions from Tealeaf
* @Response: List of Sessions
* @Path: /getactivesessions
* @Type: POST
* @Response Format: application/xml
* @Developer: Waheed Ahmed
*/
router.post('/getcontactus-replaylink', function(req, res, next) {
  console.log("BODY: ", req.body);
  req.body.username = "admin";
  req.body.password = "init";
  if(req.body && req.body.email && req.body.referer && req.body.username && req.body.password) {
    var emailId = req.body.email;
    var onlyLetterEmailId = emailId.replace(/[^a-zA-Z ]/g, " ");
    var pageUrl = 'index.php/contacts';
    if(req.body.referer.indexOf(pageUrl) != -1) {
      var onlyLetterPageUrl = pageUrl.replace(/[^a-zA-Z ]/g, " ");
    } else {
      var err = reveal.errorResponse("Page Url/Referer is not matched.");
      res.status(500).jsonp(err);
    }
    var credentials = {
      UserName: req.body.username,
      Password: req.body.password
    };
    // call helper Reveal Methods to get Sessions List. Array contains Methods. Each method runs after other in sequence.
    async.waterfall([
      async.apply(reveal.authenticate, credentials),
      reveal.verifyAuthentication,
      reveal.getSearchSortFields,
      async.apply(reveal.getContactUsFormSession, emailId, onlyLetterEmailId, pageUrl, onlyLetterPageUrl)
    ], function (err, result) {
      res.header("Content-Type",'application/json');
      if(!err) {
        if(result != null) {
          console.log("FINAL RESULT: ", result);
          res.status(200).jsonp(result);
        } else {
          res.status(200).send();
        }
      } else {
        console.log("error: ", err);
        res.status(500).jsonp(err);
      }
    });
  } else {
    var err = reveal.errorResponse("Required Fields are missing from Request Body.");
    res.status(500).jsonp(err);
  }
});

module.exports = router;
