var express = require('express');
var router = express.Router();
var request = require('request');
var xml2js = require('xml2js');
var parseString = require('xml2js').parseString;
var async = require("async");
var moment = require('moment');
var momentTz = require('moment-timezone');
var xmlBuilder = require('xmlbuilder');

const REVEAL_API_URL = 'http://192.168.16.72/PortalAPI/cxReveal.asmx/';


/* GET users listing. */
router.get('/', function(req, res, next) {

  if(req.query.email && req.query.email != null) {
    //console.log("req.query.email: ", req.query.email);
    var emailId = req.query.email;
    var onlyLetterEmailId = emailId.replace(/[^a-zA-Z ]/g, " ");

    request.post({url: REVEAL_API_URL + 'Authenticate', 
      form: {
        UserName:'admin',
        Password:'init'
      }
    }, function(err,httpResponse,body){
        if(!err) {
          //console.log.log("body: ", body);
          parseString(body, function (err1, result) {
            if(!err1) {
              //console.log.log("result: ", result);
              request.post({url: REVEAL_API_URL + 'VerifyAuthentication'}, 
                function(err,httpResponse,body){
                  if(!err) {
                    //console.log.log("body2: ", body);
                    parseString(body, function (err1, result) {
                      if(!err1) {
                        //console.log("result2: ", result);
                        request.post({url: REVEAL_API_URL + 'GetSearchSortFields'}, 
                          function(err,httpResponse,body){
                            if(!err) {
                              //console.log("body3: ", body);
                              parseString(body, function (err1, result) {
                                if(!err1) {
                                  //console.log("result3: ", result.ArrayOfTLSortField.TLSortField[1].SortID[0]);
                                  request.post({url: REVEAL_API_URL + 'StartSearch',
                                    form : {
                                      ActiveQuery: 'customvar0 contains ' +  emailId,
                                      ArchiveQuery: 'tltstscustomvar0 contains "' + onlyLetterEmailId + '"',
                                      StartDate: '7/13/2016 00:00:00',
                                      EndDate: '7/18/2016 23:59:59',
                                      SortField: 2
                                    }
                                  }, function(err,httpResponse,body){
                                      if(!err) {
                                        //console.log("body4: ", body);
                                        parseString(body, function (err1, result) {
                                          if(!err1) {
                                            //console.log("result4: ", result);
                                            var searchId = result.int._;
                                            //console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~searchId~~~~~~~~~~~~~~~~~~~~~~~: ", searchId);
                                            var count = 0;
                                            var setIntervalId = 0;
                                            async.whilst(
                                              function() { return count < 5; },
                                              function(callback) {
                                                setIntervalId =  setInterval(function() {
                                                    request.post({url: REVEAL_API_URL + 'GetSearchStatus',
                                                      form : {
                                                        SearchID: searchId
                                                      }
                                                    }, function(err,httpResponse,body){
                                                        //console.log("TATATA");
                                                        if(!err) {
                                                          //console.log("body5: ", body);
                                                          parseString(body, function (err1, result) {
                                                            //console.log("count= ", count);
                                                            if(!err1) {
                                                              //console.log("result5: ", result.TLSearchStatus.Done[0]);
                                                              //console.log("result6: ", result.TLSearchStatus.DownloadComplete[0]);
                                                              if(result.TLSearchStatus.Done[0] == 'true' && result.TLSearchStatus.DownloadComplete[0] == 'true' && count<5) {
                                                                //console.log("Inside 1");
                                                                count = 5;
                                                                callback(null, result);
                                                              } else if ((result.TLSearchStatus.Done[0] == 'false' || result.TLSearchStatus.DownloadComplete[0] == 'false') && count<5) {
                                                                //console.log("Inside 2");
                                                                count++;
                                                              } else if ((result.TLSearchStatus.Done[0] == 'false' || result.TLSearchStatus.DownloadComplete[0] == 'false') && count>=5) {
                                                                //console.log("Inside 3");
                                                                callback('respond with a resource -- fail in success', null);
                                                              }  
                                                            } else {
                                                              res.send('respond with a resource -- fail');
                                                            } 
                                                          });
                                                        }
                                                      });
                                                }, 500);
                                              },
                                              function (err, searchResp) {
                                                clearInterval(setIntervalId);
                                                if(!err) {
                                                  if(searchResp != null) {
                                                    request.post({url: REVEAL_API_URL + 'GetSearchResults',
                                                      form : {
                                                        SearchID: searchId,
                                                        MaxResults: 10
                                                      }
                                                    }, function(err,httpResponse,body){
                                                      if(!err) {
                                                        //console.log("body111: ", body);
                                                        parseString(body, function (err1, result) {
                                                          if(!err1) {
                                                            //console.log("result111: ", result);
                                                            if(result.ArrayOfTLMiniSession.TLMiniSession && result.ArrayOfTLMiniSession.TLMiniSession.length > 0){
                                                              var finalRespBody = result.ArrayOfTLMiniSession.TLMiniSession[0];
                                                              var startTime = new Date(parseInt(finalRespBody.FirstUse[0]) * 1000);
                                                              var isoStartTime = momentTz.tz(startTime, "Asia/Karachi").format();
                                                              var endTime = new Date(parseInt(finalRespBody.LastUse[0]) * 1000);
                                                              var isoEndTime = momentTz.tz(endTime, "Asia/Karachi").format();
                                                              var duration = moment.duration(moment(endTime).diff(moment(startTime)));                             
                                                              var duration_hours = duration.hours();
                                                              duration_hours = duration_hours < 10 ? '0'+ duration_hours : duration_hours;
                                                              //console.log("duration_hours: ", duration_hours);
                                                              var duration_mins = duration.minutes();
                                                              duration_mins = duration_mins < 10 ? '0'+ duration_mins : duration_mins;
                                                              //console.log("duration_mins: ", duration_mins);
                                                              var duration_seconds = duration.seconds();
                                                              duration_seconds = duration_seconds < 10 ? '0'+ duration_seconds : duration_seconds;
                                                              //console.log("duration_seconds: ", duration_seconds);
                                                              duration = duration_hours + ':' + duration_mins + ':' + duration_seconds;
                                                              //console.log("duration: ", duration);
                                                              //console.log("My startTime: ", startTime);
                                                              //console.log("My EndTime: ", endTime);
                                                              var obj1 = {
                                                                SessionId: finalRespBody.SessionId[0],
                                                                TltSid: finalRespBody.TltSid[0],
                                                                IpAddress: finalRespBody.IpAddress[0],
                                                                email: emailId,
                                                                Browser: finalRespBody.Browser[0],
                                                                StartTime: isoStartTime,
                                                                EndTime: isoEndTime,
                                                                duration: duration,
                                                                ReplayLinkBBR: 'https://www.google.com.pk/'
                                                              };
                                                              var allXml = [];
                                                              for(var i=0; i<5; i++) {
                                                                allXml.push(obj1);
                                                              }
                                                              //console.log('allXml: ', allXml);
                                                              var obj2 = {
                                                                SessionsList: {
                                                                  session: allXml
                                                                }
                                                              };
                                                              builder = new xml2js.Builder();
                                                              var xmlResp = builder.buildObject(obj2);
                                                              console.log("xmlResp: ", xmlResp);
                                                              res.send(xmlResp);
                                                            } else {
                                                              res.send();
                                                            }
                                                          } else {
                                                            res.send('respond with a resource -- fail');
                                                          } 
                                                        });
                                                      }
                                                    });
                                                  } else {
                                                    res.send();
                                                  }
                                                } else {
                                                  res.send();
                                                }
                                              }
                                            );             
                                          } else {
                                            res.send('respond with a resource -- fail');
                                          } 
                                        });
                                      }
                                    });
                                } else {
                                  res.send('respond with a resource -- fail');
                                } 
                              });
                            }
                          });
                      } else {
                        res.send('respond with a resource -- fail');
                      } 
                    });
                  }
                }); 
              } else {
                res.send('respond with a resource -- fail');
              } 
          });
        }
    });
  } else {
    res.send("Email Id not found as a Query parameter");
  }

  
});

module.exports = router;
